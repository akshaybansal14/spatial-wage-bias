import csv

header = []
data = []
is_header = True
with open("edited.csv", 'r') as file:
    reader = csv.reader(file)
    for row in reader:
        if is_header:
            header = row
            is_header = False
        else:
            data.append(row)

# col 12 = gender
# col 35 = QUALIFIED
# col 37 = State
# col 38 = country

unique_states = []
for row in data:
    if row[38] == "" and row[37] not in unique_states:
        unique_states.append(row[37])

data_states_male = []
data_states_female = []
for state in unique_states:
    data_states_male.append([])
    data_states_female.append([])
    for row in data:
        if row[37] == state:
            if int(row[35]) > 0:
                if row[12] == "Male":
                    data_states_male[-1].append(row)
                if row[12] == "Female":
                    data_states_female[-1].append(row)

# cols to be included in the final files
col_indices = [4, 6, 7, 17, 18, 19, 20, 21]

for i in range(len(unique_states)):
    with open("../data/males_" + unique_states[i][1:3] + ".csv", 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([header[i] for i in col_indices])
        for row in data_states_male[i]:
            writer.writerow([row[i] for i in col_indices])
    with open("../data/females_" + unique_states[i][1:3] + ".csv", 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([header[i] for i in col_indices])
        for row in data_states_female[i]:
            writer.writerow([row[i] for i in col_indices])

# computing the five most populated states
state_populations = []
for i in range(len(unique_states)):
    state_populations.append([unique_states[i], len(data_states_male[i]) + len(data_states_female[i]), i])
sorted_states = sorted(state_populations, key=lambda x: (-x[1], x[0]))
for i in range(5):
    with open("../data/top_states/males_" + sorted_states[i][0][1:3] + ".csv", 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([header[i] for i in col_indices])
        for row in data_states_male[sorted_states[i][2]]:
            writer.writerow([row[i] for i in col_indices])
    with open("../data/top_states/females_" + sorted_states[i][0][1:3] + ".csv", 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([header[i] for i in col_indices])
        for row in data_states_female[sorted_states[i][2]]:
            writer.writerow([row[i] for i in col_indices])
