import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.model_selection import KFold
from sklearn.linear_model import Ridge
from scipy.stats.stats import pearsonr
import matplotlib.pyplot as plt
from sklearn.model_selection import cross_val_score
from sklearn.tree import DecisionTreeRegressor

class wage_est:
    
    # constructor to initialize object
    def __init__(self, **kwargs):
        self.X_data = kwargs['covariates'] # input data
        self.Y_data = kwargs['target'] # output
        self.num_folds = kwargs['num_folds'] # number of folds for cross-validation
        self.num_data_points = len(self.X_data) # total number of data points

    # # method to fit regularized linear model
    # def lin_regularized2(self):
    #     lambda_vec = []; test_vec = []
    #     reg_constant = np.linspace(0.001, 100, 100) 		# Set of allowable regularization constants 	
    #     kf = KFold(n_splits = self.num_folds, shuffle = False)
    #     for reg_lambda in reg_constant:
    #         avg_r2_ridge = 0
    #         for train_index, test_index in kf.split(self.X_data, self.Y_data):
    #             X_train, X_test = self.X_data[train_index], self.X_data[test_index]
    #             Y_train, Y_test = self.Y_data[train_index], self.Y_data[test_index]
                    
    #             clf_ridge = Ridge(reg_lambda)
    #             clf_ridge.fit(X_train, Y_train)
    #             r2_ridge = clf_ridge.score(X_test, Y_test)
    #             avg_r2_ridge += r2_ridge

    #         avg_r2_ridge = avg_r2_ridge / self.num_folds		
    #         #print('Ridge: ' + str(reg_lambda) + ' : ' + str(avg_r2_ridge))
    #         lambda_vec.append(reg_lambda); test_vec.append(avg_r2_ridge)

    #     return lambda_vec, test_vec

    def lin_regularized(self):
        reg_constant = np.linspace(0.001, 100, 100) 		# Set of allowable regularization constants 	
        list_score = []
        for reg_lambda in reg_constant:
            clf = Ridge(reg_lambda)
            clf.fit(self.X_data, self.Y_data)
            score_val = clf.score(self.X_data, self.Y_data)
            list_score.append(score_val)

        return reg_constant, list_score

    def decision_tree(self, mean_test_pt):
        mean_test_pt = mean_test_pt + [1,1,1,1,1]
        regressor = DecisionTreeRegressor(random_state = 0)
        regressor.fit(self.X_data, self.Y_data)
        val = regressor.predict([mean_test_pt])
        print(val)
        # print(regressor.score(self.X_data, self.Y_data))
        # print(cross_val_score(regressor, self.X_data, self.Y_data, cv = 10))

        return
