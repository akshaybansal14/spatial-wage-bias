import pandas as pd
from wage_est import wage_est

def main():

    list_covariates = ['totalyearlycompensation', 'yearsofexperience', 'yearsatcompany', 'Masters_Degree', 'Bachelors_Degree', 'Doctorate_Degree'];
    df_data = pd.read_csv('../data/data AL.csv', usecols = list_covariates)
    print(df_data)


    return

if __name__ == "__main__":
    main()